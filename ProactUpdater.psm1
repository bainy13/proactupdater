function Proact-Update()
{
    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
        [Parameter(DontShow)]
        [switch]$quiet
    )

    <#  .SYNOPSIS
        version 0.1 - Written by Dave Hocking 2016.

        This script is a dirty hack to download bitbucket projects that are powershell modules
        It doesn't do anything clever with bitbucket, doesn't use Git at all, doesn't check for the need to copy/replace files
        It's inefficient, but, has low requirements and is intended as a stop-gap

        .DESCRIPTION
        If you run the script without admin rights, you are presented with the option to install into the user's personal modules folder:
        <userprofile>\documents\windowspowershell\modules
        
        If you execute the script with admin rights, the install will automatically proceed into the system's modules folder:
        <windows>\system32\windowspowershell\v1.0\modules

        ...if installed into the system, all users can access the module's functions.

        If quiet output is required, use -v at execution time

        .EXAMPLE
        Installs into either the systemroot or userprofile Module stores, depending on access rights
        
        downloadModule.ps1 

        .EXAMPLE
        Installs into either the systemroot or userprofile Module stores, depending on access rights
        Displays quiet output during execution

        downloadModule.ps1 -v
    #>

    show-header

    [string]$bitbucketAccount = "bainy13"

    # Define URL to get the files from
    [string]$bitbucket = "https://bitbucket.org"

    # Define the read-only account
    [string]$user = "proact@thehockings.net"
    [string]$pass = "pro817BUCKET+"
    [string]$pair = "${user}:${pass}"

    # Encode the credentials
    $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
    [string]$base64 = [System.Convert]::ToBase64String($bytes) 

    # Build the header
    [string]$basicAuthValue = "Basic $base64"
    [System.Collections.Hashtable]$headers = @{ Authorization = $basicAuthValue }

    # Detect if admin rights have been used
    if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] "Administrator"))
    {
        # No rights detected - install into user profile
        if (!$PSBoundParameters.ContainsKey('quiet'))
        {
            Write-Host "No Admin rights detected" -ForegroundColor Magenta
        }
        [string]$outpath = "$($env:USERPROFILE)\Documents\WindowsPowerShell\modules"
        [string]$question = read-host "Do you want to install into `"$($outpath)`" [y] or Enter to confirm"
        switch ($question)
        {
            y       {}
            ""      {}
            default {Write-Host "Stopping."; break}
        }
    }

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Admin rights detected" -ForegroundColor Green
    }
    # Admin rights used - install into system profile
    [string]$outpath = "$($env:SystemRoot)\System32\WindowsPowerShell\v1.0\Modules"

    # Set up script to be able to unzip compressed archives (PowerShell v4 lacked this feature)
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Setting up ZIP de/compression functionality..."
    }

    Update-Complete

    # Create the Projects Array, for holding the names of the projects on bitbucket
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Creating Projects Array..."
    }
    [array]$projects = @()
    $projects += "vcd"
    Update-Complete

    # Loop through the array and download the latest build from the bitbucket Server
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Looping through the projects, downloading and unpacking..."
    }
    
    foreach ($project in $projects)
    {
        Write-Host $project -ForegroundColor Yellow

        # Set the URLs/Paths to be used and download the Archive file
        $url = "$($bitbucket)/$bitbucketAccount/$($project)/get/master.zip"
        $outfile = "c:\windows\temp\$($project).zip"
        $destination = "$($outpath)\$($project)"
        Invoke-WebRequest -Uri $url -OutFile $outfile -ErrorAction Stop -ErrorVariable fatal -Headers $headers
        if ($fatal)
        {
            Write-Warning "Failed to download file - stopping process to prevent accidental removal of old versions"
            break
        }

        # Check if the folder already exists (if it does -> trash it, if not, then just extract)
        if (Test-Path $destination)
        {
            if (!$PSBoundParameters.ContainsKey('quiet'))
            {
                Write-Host "    Project already exists, trashing local copy..."
            }
            Remove-Item $destination -Confirm:$false -Recurse
        }
        Update-Unpack -project $project -outfile $outfile -destination $destination
        Update-File -destination $destination -bitbucketAccount $bitbucketAccount -project $project
        
        if(Get-Module $project){
            try{
                remove-module $project
                import-module $project
            } catch {
                Write-Host "$($project) needs to be removed and re-added manually"
            }
        }
    }
    Update-Complete
    #exit
}

function Update-Unpack()
{
    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
        [Parameter(Mandatory=$true)]
        [String]$project,

        [Parameter(Mandatory=$true)]
        [String]$outfile,

        [Parameter(Mandatory=$true)]
        [String]$destination,

        [Parameter(DontShow)]
        [switch]$quiet = $false
    )

    # Unzip the archive to the destination
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "    Working on " -NoNewline
        Write-Host "$project" -NoNewline -ForegroundColor Yellow
        Write-Host ", unzipping..."
    }

    # Create unzip function
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    [System.IO.Compression.ZipFile]::ExtractToDirectory($outfile, $destination)
}

function Update-File()
{
    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
        [Parameter(Mandatory=$true)]
        [String]$destination,

        [Parameter(Mandatory=$true)]
        [String]$bitbucketAccount,

        [Parameter(Mandatory=$true)]
        [String]$project,

        [Parameter(DontShow)]
        [switch]$quiet = $false
    )

    # The archive creates a .git folder, move the contents to the parent directory
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "    Moving files..."
    }
    Move-Item "$($destination)\$($bitbucketAccount)-$($project)-*\*" -Destination "$($destination)\"
    
    # The archive creates a .git folder, this is now useless, trash it
    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "    Cleaning up extra .git directory..."
    }
    Remove-Item "$($destination)\$($bitbucketAccount)-$($project)-*"
    Update-Complete
}

# Function declaration
function Update-Complete()
{
    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
        [Parameter(DontShow)]
        [switch]$quiet = $false
    )

    if (!$PSBoundParameters.ContainsKey('quiet'))
    {
        Write-Host "Done." -ForegroundColor Green
        ""
    }
}

function show-header()
{
    Write-Host "                                           " -ForegroundColor White -BackgroundColor Red
    Write-Host " ██████ █████   ████   ████   ████ ██████  " -ForegroundColor White -BackgroundColor Red
    Write-Host " ██  ██ ██  ██ ██  ██ ██  ██ ██      ██    " -ForegroundColor White -BackgroundColor Red
    Write-Host " ██████ █████  ██  ██ ██████ ██      ██    " -ForegroundColor White -BackgroundColor Red
    Write-Host " ██     ██  ██ ██  ██ ██  ██ ██      ██    " -ForegroundColor White -BackgroundColor Red
    Write-Host " ██     ██  ██  ████  ██  ██  ████   ██    " -ForegroundColor White -BackgroundColor Red
    Write-Host "                                           " -ForegroundColor White -BackgroundColor Red
    Write-Host "         delivering  business  agility     " -ForegroundColor White -BackgroundColor Red
    Write-Host "                                           " -ForegroundColor White -BackgroundColor Red
    ""
}
